from matplotlib.patches import Rectangle
from .mplbase import MPLBase


class SpanRectangle(MPLBase):
    """
    Provide the visualization for creating a PolyRectangle.
    """

    def __init__(self, parent=None, world=False):
        """
        Initialize all important variables for matplotlib drawing.

        Parameters
        ----------
        parent: core.PolygonHandler
        world: bool [False]
            Flag if the clicked action was "PolyWorld".
        """
        super(SpanRectangle, self).__init__(parent)
        self.world = world

    def _reset(self):
        ec = '#ff0000' if self.world else '#000000'
        self.artist = Rectangle((0, 0), 0, 0, fc='none', ec=ec)
        self.figure.ax.add_patch(self.artist)

    def onPress(self, event):
        """Collect the data of the starting corner of the rectangle."""
        self._reset()
        if event.inaxes != self.artist.axes:
            return
        if event.button is 1:  # left mouse button
            self.x_p = self.curr_x
            self.y_p = self.curr_y
            self.background = self.figure.canvas.copy_from_bbox(self.artist.axes.bbox)

    def onMotion(self, event):
        """Resize the helper rectangle while spanning."""
        # since the artist is only instanciated when clicked it becomes
        # necessary to check against it when moving the cursor onto the
        # canvas
        if not hasattr(self, 'artist'):
            return
        if event.inaxes != self.artist.axes:
            return
        try:
            x_m = self.curr_x
            y_m = self.curr_y
            self.artist.set_width(x_m - self.x_p)
            self.artist.set_height(y_m - self.y_p)
            self.artist.set_xy((self.x_p, self.y_p))
            self.update()
        except (AttributeError, TypeError):
            pass

    def onRelease(self, event):
        """
        Restore the canvas and empty the rectangles data.

        Note
        ----
        The path *self.artist* will be removed in MPLBase.drawToCanvas()
        """
        if event.inaxes != self.artist.axes:
            return
        try:
            self.x_r = self.curr_x
            self.y_r = self.curr_y

            # draw the actually spanned rectangle
            rect = Rectangle((0, 0), 0, 0, fc='none', lw=1, ec='blue')
            rect.set_width(self.x_r - self.x_p)
            rect.set_height(self.y_r - self.y_p)
            rect.set_xy((self.x_p, self.y_p))

            if self.world:
                self.parent.drawPoly(
                    'world', rect, {
                        'start': [self.x_p, self.y_p],
                        'end': [self.x_r, self.y_r],
                        'marker': None,
                        'area': 0.0,
                        'layers': None,
                        'worldMarker': True,
                    })
            else:
                self.parent.drawPoly(
                    'rectangle', rect, {
                        'start': [self.x_p, self.y_p],
                        'end': [self.x_r, self.y_r],
                        'pos': None,
                        'size': None,
                        'marker': None,
                        'area': 0.0,
                        'boundaryMarker': 1,
                        'leftDirection': True,
                        'isHole': False,
                        'isClosed': True,
                    })
        except AttributeError:
            pass


if __name__ == '__main__':
    pass
