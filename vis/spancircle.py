#!/usr/bin/env python
# encoding: UTF-8

from matplotlib.patches import Circle, Polygon
import numpy as np

from .mplbase import MPLBase
import pygimli as pg
from pygimli.meshtools import polytools as plc


class SpanCircle(MPLBase):
    """
    Provide the visualization for the creation of a polyCircle.
    """

    def __init__(self, parent=None):
        """Initialize all important variables for matplotlib drawing."""
        super(SpanCircle, self).__init__(parent)

    def _reset(self):
        # dummy to be drawn and 'exported' later
        self.artist = Circle((0, 0), 0, fc='none', ec='#000000')
        # bring the dummy on the canvas
        self.figure.ax.add_patch(self.artist)

    def distance(self):
        """Calculate the radius from the press and release event."""
        return np.sqrt((self.x_m - self.x_p)**2 + (self.y_m - self.y_p)**2)

    def onPress(self, event):
        """
        Collect x,y-positions from the event and prepare the canvas for drawing
        while dragging.
        """
        self._reset()
        if event.inaxes != self.artist.axes:
            return
        if event.button is 1:
            self.x_p = self.curr_x
            self.y_p = self.curr_y
            self.background = self.figure.canvas.copy_from_bbox(self.artist.axes.bbox)
            self.update()

    def onMotion(self, event):
        """Resize the helper circle while spanning."""
        if not hasattr(self, 'artist'):
            return
        if event.inaxes != self.artist.axes:
            return
        try:
            self.x_m = self.curr_x
            self.y_m = self.curr_y
            # inconsistent mpl stuff
            self.artist.center = (self.x_p, self.y_p)
            self.artist.set_radius(self.distance())
            # TODO: show radius on the center while dragging
            # self.figure.ax.annotate(self.distance(), xy=(self.x_p, self.y_p))
            self.update()
        except (AttributeError, TypeError):
            pass

    def onRelease(self, event):
        """Restore the canvas and empty the circles data."""
        if event.inaxes != self.artist.axes:
            return
        try:
            # inconsistent mpl stuff
            self.artist.center = (0, 0)
            self.artist.set_radius(0)
            self.artist.axes.draw_artist(self.artist)
            # draw the spanned circle
            pcirc = plc.createCircle(
                pos=(self.x_p, self.y_p), radius=self.distance())
            circ = Polygon([[pg.x(pcirc)[i], pg.y(pcirc)[i]]
                            for i in range(len(pg.x(pcirc)))])
            self.parent.drawPoly('circle', circ, {
                'radius': self.distance(),
                'pos': [self.x_p, self.y_p],
                'segments': 12,
                'start': 0.0,
                'end': 2*np.pi,
                'marker': None,
                'area': 0.0,
                'boundaryMarker': 1,
                'leftDirection': True,
                'isHole': False,
                'isClosed': True
            })
        except AttributeError:
            pass


if __name__ == '__main__':
    pass
