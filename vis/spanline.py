#!/usr/bin/env python

from matplotlib.lines import Line2D
from .mplbase import MPLBase
import numpy as np


class SpanLine(MPLBase):
    """
    Provide the visualization for creating a PolyLine.
    """

    def __init__(self, parent=None):
        """
        Initialize all important variables for matplotlib drawing.

        Parameters
        ----------
        parent: core.PolygonHandler
        """
        super(SpanLine, self).__init__(parent)
        self._reset()

    def _reset(self):
        # dummy to be drawn and 'exported' later
        self.artist = Line2D([], [], c='#000000')
        # dummy line to portrait where the line will be
        self.mline = Line2D([], [], lw=1)
        # add both lines to canvas
        self.figure.ax.add_line(self.artist)
        self.figure.ax.add_line(self.mline)
        # store clicked data points
        self.x = []
        self.y = []
        # counter for plotting a line when there two sets of coordinates
        self.clicker = -1

    def onPress(self, event):
        """Collect the event data to later pass on to the PolyLine."""
        if event.button is 1:
            if event.dblclick:  # stop line building on double click
                self._reset()
            else:
                # the helper line is only need while dragging
                self.mline.set_data([], [])
                # append the 'snapped' cursor position
                self.x.append(self.curr_x)
                self.y.append(self.curr_y)
                # set the line data
                self.artist.set_data(self.x, self.y)
                self.clicker += 1
                self.background = self.figure.canvas.copy_from_bbox(
                    self.figure.ax.bbox)
                self.update()

    def onMotion(self, event):
        """Set the data of cursor motion to the helper line."""
        try:  # to draw this stuff
            self.mline.set_data(
                (self.x[-1], self.curr_x), (self.y[-1], self.curr_y))
        except (AttributeError, IndexError, TypeError):
            pass

    def onRelease(self, event):
        """Reset the line and send the necessary data to build the PolyLine."""
        try:  # to drag the line with the cursor
            self.background = self.figure.canvas.copy_from_bbox(
                self.mline.axes.bbox)
            self.update()
        except (IndexError, TypeError):
            pass

        if self.clicker > 0:
            self.artist.set_data([0], [0])
            line = Line2D(
                [self.x[self.clicker - 1], self.x[self.clicker]],
                [self.y[self.clicker - 1], self.y[self.clicker]]
                )
            self.parent.drawPoly('line', line, {
                'start': [self.x[self.clicker - 1], self.y[self.clicker - 1]],
                'end': [self.x[self.clicker], self.y[self.clicker]],
                'segments': 1,
                'boundaryMarker': 1,
                'leftDirection': True
                })


if __name__ == '__main__':
    pass
