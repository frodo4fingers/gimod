#!/usr/bin/env python
# encoding: UTF-8

from matplotlib.patches import Polygon
from matplotlib.lines import Line2D
from .mplbase import MPLBase


class SpanPoly(MPLBase):
    """Provide the visualization for creating a manually drawn Polygon."""

    def __init__(self, parent=None):
        """
        Initialize all important variables for matplotlib drawing.

        Parameters
        ----------
        parent: core.PolygonHandler
        """
        super(SpanPoly, self).__init__(parent)
        # if hasattr(self.parent, 'grid'):
        #     self.parent.grid.update()
        self._reset()

    def _reset(self):
        """
        Set helper line, the line that assembles Polygon parts and the storages
        lists for establishing those lines.
        """
        self.artist = Line2D([], [], c='#000000')
        self.poly = Line2D([], [], c='#000000')
        # NOTE. reminder to self: setting the active one (artist) to animated
        # (True) results in constant annoying flickering
        # self.artist.set_animated(True)
        self.figure.ax.add_line(self.artist)
        self.figure.ax.add_patch(self.poly)
        # store the clicked data points in lists
        self.x = []
        self.y = []

    def onPress(self, event):
        """
        Store the data of first click and enable the drawing of the helper
        line. Clsoing the polygon happens with doubleclick that sends the x and
        y values to the builder.
        """
        self.background = self.figure.canvas.copy_from_bbox(
            self.figure.ax.bbox)

        if event.button is 1:  # left mouse button
            coords = [[self.x[i], self.y[i]] for i in range(len(self.x))]
            if event.dblclick:  # close polygon
                poly = Polygon(coords)
                self.parent.drawPoly('polygon', poly, {
                    'verts': coords,
                    'boundaryMarker': 1,
                    'leftDirection': True,
                    'marker': None,
                    'area': 0.0,
                    'isHole': False,
                    'isClosed': True
                    })
                # reset the necessary components
                self._reset()

            else:  # append point to polygon
                self.x.append(self.curr_x)
                self.y.append(self.curr_y)
                # draw the edge between two points
                self.poly.set_data(self.x, self.y)
                self.update()

    def onMotion(self, event):
        """Set the data of cursor motion to the helper line."""
        try:  # to draw this stuff
            self.artist.set_data(
                [self.x[-1], self.curr_x], [self.y[-1], self.curr_y])
            self.update()
        except (AttributeError, IndexError, TypeError):
            pass

    def onRelease(self, event):
        """Reset the data of the line to draw it from scratch."""
        try:  # to drag the line with the cursor
            self.artist.set_data([], [])
            self.background = self.figure.canvas.copy_from_bbox(
                self.figure.ax.bbox)
            self.update()
        except (IndexError, TypeError):
            pass


if __name__ == '__main__':
    pass
