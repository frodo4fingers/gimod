"""
The matplotlib basiscs for the polygon behaviour.
"""

from matplotlib import pyplot as plt
from matplotlib import patches
from matplotlib import lines
import numpy as np

from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QCursor
from PyQt5.QtCore import Qt, pyqtSignal


class MPLBase():
    """Hold the general behaviour of the drawn polygons."""

    def __init__(self, parent=None):
        """."""
        self.parent = parent  # PolygonHandler
        self.figure = parent.plotwidget

    def connect(self):
        """."""
        self.cid_p = self.figure.canvas.mpl_connect(
            'button_press_event', self.onPress)
        self.cid_dp = self.figure.canvas.mpl_connect(
            'button_press_event', self.onPress)
        self.cid_mm = self.figure.canvas.mpl_connect(
            'motion_notify_event', self.onMotion)
        self.cid_mg = self.figure.canvas.mpl_connect(
            'motion_notify_event', self._magnetGrid)
        self.cid_r = self.figure.canvas.mpl_connect(
            'button_release_event', self.onRelease)
        self.cid_ae = self.figure.canvas.mpl_connect(
            'axes_enter_event', self.axesEnter)
        self.cid_al = self.figure.canvas.mpl_connect(
            'axes_leave_event', self.axesLeave)

    def disconnect(self, cid=None):
        """Disconnect all the stored connection ids."""
        if cid is not None:
            self.figure.canvas.mpl_disconnect(getattr(self, cid))
        else:
            for cid in ['cid_p', 'cid_dp', 'cid_mm', 'cid_r', 'cid_ae', 'cid_al']:
                if hasattr(self, cid):
                    self.disconnect(cid)
        
        if hasattr(self, 'cp'):
            try:
                self.cp.remove()
            except ValueError:
                pass
    
    def axesLeave(self, event):
        """Allow the standard cursors outside the drawing area."""
        QApplication.restoreOverrideCursor()

    def axesEnter(self, event):
        """Prepend matplotlibs new feature to create a QWaitCursor while dragging some stuff."""
        QApplication.setOverrideCursor(QCursor(Qt.ArrowCursor))

    def drawToCanvas(self, patches):
        """Convenience function to add the gathered path to the current figure."""
        [p.remove() for p in reversed(self.figure.ax.patches)]
        [l.remove() for l in reversed(self.figure.ax.lines)]
        cmap = plt.cm.get_cmap('Set3', len(patches))
        alpha = self.parent._alpha if hasattr(self.parent, '_alpha') else 1
        for i, patch in enumerate(patches):
            # patch.set_fc((*cmap(i)[:3], 0.8))
            if isinstance(patch, lines.Line2D):
                patch.set_lw(2)
                patch.set_color((*cmap(i)[:-1], alpha))
                self.figure.ax.add_line(patch)
            else:
                patch.set_lw(1)
                patch.set_fc((*cmap(i)[:-1], alpha))
                patch.set_ec('#000000')
                self.figure.ax.add_patch(patch)

    def update(self):
        """Method to flush the drawing event of each PolyTool."""
        self.figure.canvas.restore_region(self.background)
        self.artist.axes.draw_artist(self.artist)
        self.figure.canvas.blit(self.artist.axes.bbox)

    def _magnetGrid(self, event):
        """.

        Parameters
        ----------
        event: MPL MouseEvent
        """
        if event.inaxes:
            self.curr_x = round(event.xdata, self.parent.parent.precision)
            self.curr_y = round(event.ydata, self.parent.parent.precision)

            try:
                self.cp.remove()
            except (ValueError, AttributeError):
                # unknown reasons
                pass
            self.parent.parent.parent.w_xyposition.setText(
                "x: {}, y: {}".format(self.curr_x, self.curr_y)
                )
            self.cp = self.figure.ax.scatter(
                self.curr_x, self.curr_y, c='#ff0000', marker='+', zorder=100, lw=0.5)
            self.figure.canvas.draw()
