import sys

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib.pyplot import tight_layout

import numpy as np
import random

from scipy.misc import imread
from PyQt5.QtWidgets import QPushButton, QVBoxLayout, QWidget, QStackedLayout, QHBoxLayout, QToolBar

import pygimli as pg


class PlotWindow(QWidget):
    def __init__(self, parent=None):
        super(PlotWindow, self).__init__(parent)
        self.parent = parent  # GIMod
        self.setupWidget()

    def setupWidget(self):
        # layout for that widget
        lyt_tb = QStackedLayout()

        # for MPL
        self.stack = QStackedLayout()
        self.figure, self.canvas, self.ax, tw = self.newFigureWidget()
        self.stack.addWidget(self.canvas)
        lyt_tb.addWidget(tw)
        _w = QWidget()

        # for the PLC
        self.p_fig, self.p_canv, self.p_ax, p_tw = self.newFigureWidget()
        self.stack.addWidget(self.p_canv)
        lyt_tb.addWidget(p_tw)

        # for the MESH
        self.m_fig, self.m_canv, self.m_ax, m_tw = self.newFigureWidget()
        self.stack.addWidget(self.m_canv)
        lyt_tb.addWidget(m_tw)

        self.stack.setCurrentIndex(0)
        lyt_tb.setCurrentIndex(0)
        # widget for the toolbar
        w_tb = QWidget()
        w_tb.setLayout(lyt_tb)

        # get the stackedlayout-widget into the toolbar
        self.parent.toolbar.addToToolBar(w_tb)
        self.setLayout(self.stack)

    def newFigureWidget(self):
        fig = Figure()
        cnvs = FigureCanvas(fig)
        tb = NavigationToolbar(cnvs, None)
        tb.hide()
        ax = self._initPlot(fig, cnvs)

        acns = QToolBar()
        for acn in tb.actions():
            if not acn.isSeparator() and acn.text() != '':
                acns.addAction(acn)
        w = QWidget()
        l = QHBoxLayout()
        l.addWidget(acns)
        l.setContentsMargins(0, 0, 0, 0)
        w.setLayout(l)

        return fig, cnvs, ax, w

    def showPoly(self, poly):
        ax = self.p_ax
        ax.clear()
        pg.show(poly, ax=ax)
        # show(self.mesh, pg.solver.parseArgToArray(
        #     attr_map, self.mesh.cellCount(), self.mesh),
        #     ax=self.plot_mesh.axis
        #     )
        # show(drawMeshBoundaries(
        #     self.plot_mesh.axis, self.mesh, hideMesh=False),
        #     ax=self.plot_mesh.axis,
        #     fillRegion=False
        # )
        self.p_canv.draw()
        self.stack.setCurrentIndex(1)
        self.parent.toolbar.navbars.setCurrentIndex(1)
        self.parent.toolbar.cbbx_figs.setCurrentIndex(1)

    def showMesh(self, mesh):
        ax = self.m_ax
        ax.clear()
        pg.show(mesh, ax=ax)
        self.m_canv.draw()
        self.stack.setCurrentIndex(2)
        self.parent.toolbar.navbars.setCurrentIndex(2)
        self.parent.toolbar.cbbx_figs.setCurrentIndex(2)

    def _initPlot(self, fig, cnvs):
        """plot some random stuff"""
        # create an axis
        ax = fig.add_subplot(111)
        ax.set_aspect('equal')
        ax.set_xlim([-5, 5])
        ax.set_ylim([-5, 0])
        ax.grid(alpha=0.5, lw=0.5)
        fig.set_tight_layout(True)
        # refresh canvas
        cnvs.draw()
        return ax

    def setBackgroundImage(self, fpath):
        """
        .

        Parameters
        ----------
        fpath: str
            Path to the image that will be loaded as background
        """
        self.global_alpha = 0.5
        img = imread(fpath, flatten=True)
        _y, _x = img.shape
        self.bimg = self.ax.imshow(np.flipud(img), cmap='gray')
        self.ax.set_xlim([0, _x])
        self.ax.set_ylim([0, _y])
        self.canvas.draw()

    def removeBackground(self):
        self.bimg.remove()
        self.canvas.draw()


if __name__ == '__main__':
    pass
