#!/usr/bin/env python

import os
import psutil
import sys
import threading
import time

from PyQt5.QtWidgets import (
    QMainWindow, QApplication, QStatusBar, QSplitter, QLabel, QWidget,
    QFileDialog
    )
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon

from pygimli.meshtools import exportPLC

from widgets.toolbar import GIModToolBar
from widgets.tabholder import TabHolder
from vis.plotwindow import PlotWindow


class GIMod(QMainWindow):

    def __init__(self, parent=None):
        """.
        """
        super(GIMod, self).__init__(parent)
        self.setupWidget()

        # connect
        # self.toolbar.cbbx_accuracy.currentIndexChanged.connect(self.plotwindow.updateGrid)

    def setupWidget(self):
        """Set the GUI together from the other widgets."""
        # get the icon path
        self.ipath = os.path.join(os.path.dirname(__file__), 'icons')
        # instanciate the status bar to prompt some information of what is
        # going on beneath the hood
        self.statusbar = QStatusBar()
        self.statusbar.addPermanentWidget(MemoryUsage())
        self.setStatusBar(self.statusbar)

        # instanciate the toolbar with the polytool functionality
        self.toolbar = GIModToolBar(self)
        self.addToolBar(self.toolbar)

        self.plotwindow = PlotWindow(self)
        # _plotwidget = QWidget()
        # _plotwidget.setLayout(self.plotwindow)

        self.tabholder = TabHolder(self)

        self.toolbar.ph.setPlotWidget(self.plotwindow)
        # tile the GUI in two resizable sides
        splitter = QSplitter(Qt.Horizontal)
        splitter.addWidget(self.tabholder)
        splitter.addWidget(self.plotwindow)
        # splitter.addWidget(_plotwidget)

        self.setCentralWidget(splitter)

        self.statusbarWidgets()

        # self.setGeometry(1500, 100, 1000, 600)
        # window name
        self.setWindowTitle("GIMod")
        self.setWindowIcon(QIcon('icons/logo.png'))
        self.show()

    def statusbarWidgets(self):
        self.w_xyposition = QLabel()

        self.statusbar.addWidget(self.w_xyposition)

    def createAndSavePLC(self):
        plc = self.toolbar.ph.getPLC()
        fname = QFileDialog.getSaveFileName(caption='Save as .plc')[0]
        if fname is not '':
            if not fname.endswith('.plc'):
                fname += '.plc'
            exportPLC(plc, fname)

    def saveMesh(self):
        mesh = self.tabholder.mopttab.mesh
        fname = QFileDialog.getSaveFileName(caption='Save as .bms')[0]
        if fname is not '':
            if not fname.endswith('.bms'):
                fname += '.bms'
            mesh.save(fname)


class MemoryUsage(QLabel):
    """
    Class in external thread to display the memory usage of pynet.

    The run() method will be started and it will run in the background
    until the application exits.
    """

    def __init__(self, interval=5, parent=None):
        """
        Constructor.
        """
        super(MemoryUsage, self).__init__(parent)
        # the time span a refresh will take
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True  # Daemonize thread
        thread.start()  # Start the execution

    def run(self):
        """Method that runs forever."""
        while True:
            process = psutil.Process(os.getpid())
            mem = self.humanReadable(process.memory_info().rss)
            self.setText("MEM " + mem)
            time.sleep(self.interval)

    def humanReadable(self, num, suffix='B'):
        """
        Will shrink down the KiloBytes into human readyble form.
        taken from
        https://stackoverflow.com/questions/1094841/reusable-library-to-get-human-readable-version-of-file-size
        """
        for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
            if abs(num) < 1024.0:
                return "%3.1f%s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f%s%s" % (num, 'Yi', suffix)



if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setApplicationName("GIMod")

    main = GIMod()
    main.show()

    sys.exit(app.exec_())
