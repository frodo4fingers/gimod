from collections import defaultdict
import os

from matplotlib.patches import Rectangle, Polygon
from matplotlib.lines import Line2D

import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from vis.spanrectangle import SpanRectangle
from vis.spancircle import SpanCircle
from vis.spanline import SpanLine
from vis.spanpoly import SpanPoly

import pygimli as pg
import pygimli.meshtools as mt


class PolygonHandler():
    """
    Handles triggering the differnet tools and takes care that the object
    parameters are sent to the correct location.
    """

    def __init__(self, parent=None):
        """

        Parameters
        ----------
        parent: GIModToolBar
        """
        self.parent = parent  # GIModToolBar
        self.all_polys = {}
        self.undone = []

    def setPlotWidget(self, widget):
        self.plotwidget = widget

    def triggerPolyWorld(self, checked):
        """
        Instanciate the MPL PolyWorld to draw the model tank.

        Parameters
        ----------
        checked: bool
            The value if the action of polyworld whether is checked or not.
        """
        if checked:
            if hasattr(self, 'active_polytool'):
                self.active_polytool.disconnect()
                self.active_poly_btn.setChecked(False)
            
            self.active_poly_btn = self.parent.acn_polyworld

            self.active_polytool = SpanRectangle(self, world=True)
            self.active_polytool.connect()
        else:
            self.active_polytool.disconnect()

    def triggerPolyRectangle(self, checked):
        """
        Instanciate the MPL PolyRectangle.

        Parameters
        ----------
        checked: bool
            The value if the action of rectangle whether is checked or not.
        """
        if checked:
            if hasattr(self, 'active_polytool'):
                self.active_polytool.disconnect()
                self.active_poly_btn.setChecked(False)

            self.active_poly_btn = self.parent.acn_polyrect

            self.active_polytool = SpanRectangle(self)
            self.active_polytool.connect()
        else:
            self.active_polytool.disconnect()

    def triggerPolyCircle(self, checked):
        """
        Instanciate the MPL PolyCircle.

        Parameters
        ----------
        checked: bool
            The value if the action of circle whether is checked or not.
        """
        if checked:
            if hasattr(self, 'active_polytool'):
                self.active_polytool.disconnect()
                self.active_poly_btn.setChecked(False)
            
            self.active_poly_btn = self.parent.acn_polycirc

            self.active_polytool = SpanCircle(self)
            self.active_polytool.connect()
        else:
            self.active_polytool.disconnect()

    def triggerPolyLine(self, checked):
        """
        Instanciate the MPL PolyLine.

        Parameters
        ----------
        checked: bool
            The value if the action of line whether is checked or not.
        """
        if checked:
            if hasattr(self, 'active_polytool'):
                self.active_polytool.disconnect()
                self.active_poly_btn.setChecked(False)

            self.active_poly_btn = self.parent.acn_polyline

            self.active_polytool = SpanLine(self)
            self.active_polytool.connect()
        else:
            self.active_polytool.disconnect()

    def triggerPolygon(self, checked):
        """
        Instanciate the MPL PolyLine.

        Parameters
        ----------
        checked: bool
            The value if the action of line whether is checked or not.
        """
        if checked:
            if hasattr(self, 'active_polytool'):
                self.active_polytool.disconnect()
                self.active_poly_btn.setChecked(False)

            self.active_poly_btn = self.parent.acn_polygon

            self.active_polytool = SpanPoly(self)
            self.active_polytool.connect()
        else:
            self.active_polytool.disconnect()

    def setPolyAlpha(self, alpha):
        """The opacity value for the drawn polygons."""
        self._alpha = alpha
        if hasattr(self, 'active_polytool'):
            [
                p.set_alpha(alpha) for p in self.active_polytool.figure.ax.patches
            ]

    def drawPoly(self, kind, patch, params):
        # print(kind, poly, params)
        if kind == 'world' and 'world' not in self.all_polys.keys():
            self.all_polys['world_0'] = {
                'patch': patch,
                'data': params
                }
            self.triggerPolyWorld(False)
            self.parent.acn_polyworld.setChecked(False)
            self.parent.acn_polyworld.setEnabled(False)
        else:
            num = len([n for n in self.all_polys.keys() if kind in n])
            self.all_polys[kind.lower() + '_' + str(num)] = {
                'patch': patch,
                'data': params
            }

        # pass the new poly data to parametertree
        gimod = self.parent.parent
        gimod.tabholder.pttab.addPolyParameters(kind, params)
        # send all patches to drawing method in MPLBase
        # they are all redrawn for the change in marker color and for transparency reasons when drawing track
        patches = [v['patch'] for v in self.all_polys.values()]
        self.active_polytool.drawToCanvas(patches)

    def updateAndRedrawPatches(self):
        """
        Draw the changed Patches.

        When the parametertree was changed and btn_refresh was clicked, this method is triggered by
        gimod.tabholder.pttab.createPolyDict(). The parameters in self.all_polys are taken and the
        suitable ones will be used to update the stored patches.
        """
        patches = []
        for v in self.all_polys.values():
            patch = v['patch']
            if isinstance(patch, Line2D):
                x1, y1 = v['data']['start']
                x2, y2 = v['data']['end']
                patch.set_data((x1, x2), (y1, y2))

            elif isinstance(patch, Rectangle):
                x1, y1 = v['data']['start']
                x2, y2 = v['data']['end']
                patch.set_width(abs(x2 - x1))
                patch.set_height(y2 - y1)
                patch.set_xy((x1, y1))

            elif isinstance(patch, Polygon):
                if 'verts' in v['data'].keys():  # Polygon
                    xy = v['data']['verts']
                    patch.set_xy(xy)
                else:  # Circle
                    pcirc = mt.createCircle(**v['data'])
                    patch = Polygon([[pg.x(pcirc)[i], pg.y(pcirc)[i]]
                                for i in range(len(pg.x(pcirc)))])
            patches.append(patch)

        self.active_polytool.drawToCanvas(patches)

    def setPatchActive(self, oid):
        # print(oid)
        path = self.all_polys[oid]['patch']
        print(self.all_polys)

    def createPLC(self):
        """pyGIMLi PLC."""
        # get possible updates from the parametertree and ensure the right
        # type of all variables
        gimod = self.parent.parent
        gimod.tabholder.pttab.createPolyDict()

        polys = []
        for kind, v in self.all_polys.items():
            kind = kind.split('_')[0]
            # print("*"*5, kind, v)
            kind = kind[0].upper() + kind[1:]
            # call the appropiate method to build poly
            plc = getattr(mt, 'create' + kind)
            polys.append(plc(**v['data']))

        # merge all polys into one
        self.poly = mt.mergePLC(polys)
        # show/update poly figure
        gimod = self.parent.parent
        gimod.plotwindow.showPoly(self.poly)
        # set the mesh button active since there is now a polygon to begin with
        gimod.tabholder.mopttab.btn_mesh.setEnabled(True)
        gimod.tabholder.pttab.btn_saveplc.setEnabled(True)

    def getPLC(self):
        if not hasattr(self, 'poly'):
            self.createPLC()
        return self.poly

    def undoLastPoly(self):
        """
        Undo last created Polygon figure.

        Note
        ----
        Called by gimod.tabholder.pttab.btn_undo
        """
        _pt = self.parent.parent.tabholder.pttab
        # this will cast everything right
        _pt.createPolyDict()
        # get last key
        pid = list(self.all_polys.keys())[-1]
        # allow recreation of world
        if pid == 'world_0':
            self.parent.acn_polyworld.setEnabled(True)
        # get the last added item
        last_added = self.all_polys.pop(pid)
        # get the general type of polygon
        poly_type = pid.split('_')[0]
        # save for redo
        self.undone.append({poly_type: last_added})
        # delete item from parametertree
        _pt.deleteItemByPID(pid)
        # make the redo button accessible
        _pt.btn_redo.setEnabled(True)
        # redraw stuff
        self.updateAndRedrawPatches()


    def redoLastPoly(self):
        poly = self.undone.pop()
        if len(self.undone) == 0:
            _pt = self.parent.parent.tabholder.pttab
            _pt.btn_undo.setEnabled(False)
            _pt.btn_redo.setEnabled(False)

        kind = list(poly.keys())[0]
        self.drawPoly(kind, poly[kind]['patch'], poly[kind]['data'])


if __name__ == '__main__':
    pass
