from PyQt5.QtWidgets import QTabWidget

from .tabs.tabimage import ImageTab
from .tabs.parametertree import ParameterTree
from .tabs.meshoptions import MeshOptions


class TabHolder(QTabWidget):

    def __init__(self, parent=None):
        super(TabHolder, self).__init__(parent)
        self.parent = parent  # GIMod
        self.setup()

    def setup(self):
        """Design the Layout."""
        self.setTabPosition(QTabWidget.West)

        self.pttab = ParameterTree(self)
        self.mopttab = MeshOptions(self)
        self.imagetab = ImageTab(self)

        self.addTab(self.pttab, 'Parameters')
        self.addTab(self.mopttab, 'Mesh Options')
        self.addTab(self.imagetab, 'Image Scan')
