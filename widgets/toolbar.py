from PyQt5.QtWidgets import (
    QToolBar, QAction, QGroupBox, QWidget, QHBoxLayout, QComboBox, QPushButton,
    QVBoxLayout
)
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSize

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from core.polygonhandler import PolygonHandler


class GIModToolBar(QToolBar):

    def __init__(self, parent=None):
        """
        .

        Parameters
        ----------
        parent: GIMod
        """
        super(GIModToolBar, self).__init__(parent)
        self.parent = parent  # GIMod
        self.setupWidget()

        self.cbbx_accuracy.currentIndexChanged.connect(self.updatePrecision)

        self.ph = PolygonHandler(self)
        self.acn_polyworld.triggered.connect(self.ph.triggerPolyWorld)
        self.acn_polyrect.triggered.connect(self.ph.triggerPolyRectangle)
        self.acn_polycirc.triggered.connect(self.ph.triggerPolyCircle)
        self.acn_polyline.triggered.connect(self.ph.triggerPolyLine)
        self.acn_polygon.triggered.connect(self.ph.triggerPolygon)

        self.cbbx_figs.currentIndexChanged.connect(self.switchFigure)

    def setupWidget(self):
        """Set Layout."""
        self.ipath = self.parent.ipath + '/'
        self.cbbx_accuracy = QComboBox()
        self.cbbx_accuracy.setToolTip("Precision on cursor snap")
        self.cbbx_accuracy.addItems(
            ["100", "10", "1", "0.1", "0.01", "0.001", "0.0001"]
            )
        self.cbbx_accuracy.setCurrentIndex(3)

        self.addWidget(self.cbbx_accuracy)
        self.addSeparator()
        self._createPolygonActions()
        self.addSeparator()
        self._createPlotSwitch()
        # here the MPL toolbar is added
        self.addHStretch()
        self.updatePrecision()

        self.setFixedHeight(35)
        self.setIconSize(QSize(28, 28))

    def addHStretch(self):
        w = QWidget()
        self.h = QHBoxLayout()
        self.h.addStretch(10)
        w.setLayout(self.h)
        self.addWidget(w)

    def _createPolygonActions(self):
        """Subbed to keep overview as light as possible."""
        self.acn_polyworld = QAction(
            QIcon(self.ipath + 'poly_world.svg'), 'PolyWorld',
            checkable=True
            )
        self.acn_polyrect = QAction(
            QIcon(self.ipath + 'poly_rectangle.svg'), 'PolyRectangle',
            checkable=True
            )
        self.acn_polycirc = QAction(
            QIcon(self.ipath + 'poly_circle.svg'), 'PolyCircle',
            checkable=True
            )
        self.acn_polyline = QAction(
            QIcon(self.ipath + 'poly_line.svg'), 'PolyLine',
            checkable=True
            )
        self.acn_polygon = QAction(
            QIcon(self.ipath + 'poly_gon.svg'), 'Polygon',
            checkable=True
            )
        self.addAction(self.acn_polyworld)
        self.addAction(self.acn_polyrect)
        self.addAction(self.acn_polycirc)
        self.addAction(self.acn_polyline)
        self.addAction(self.acn_polygon)

    def _createPlotSwitch(self):
        """
        Create two buttons that allow to switch between thethree stages of model creation.

        That would be the MPL figure, the pyGIMLi PLC and the pyGIMLi Mesh
        """
        self.cbbx_figs = QComboBox()
        self.cbbx_figs.addItems(["MPL", "PLC", "Mesh"])
        self.cbbx_figs.setCurrentIndex(0)

        self.addWidget(self.cbbx_figs)

    def addToToolBar(self, widget):
        """
        Addthe action from the foreign navigationtoolbar of matplotlib.

        Parameters
        ----------
        widget: QWidget
            Widget containing the StackedLayout.

        Note
        ----
        Called from vis.plotwindow.py
        """
        self.navbars = widget.layout()
        self.addWidget(widget)

    def updatePrecision(self):
        """
        Calculate click precision for drawing Polygons.

        The QComboBox from Toolbar is taken the positions around the comma is counted. From that
        the precision is derived.
        """
        _precision = self.cbbx_accuracy.currentText()
        pts = _precision.split('.')
        if len(pts) == 2:
            pts = pts[1]
            self.precision = len(pts)

        else:
            self.precision = -(len(*pts) - 1)

    def switchFigure(self, idx):
        """
        Set the chosen layer active/visible.

        This triggers the PlotWindow's QStackedLayout to set the index
        accordingly. Same procedure for the toolbars MPL navbar.

        Parameters
        ----------
        idx: int
            Sent by signal indicating the current index of the QComboBox.
        """
        stack = self.parent.plotwindow.stack
        stack.setCurrentIndex(idx)
        self.navbars.setCurrentIndex(idx)


if __name__ =='__main__':
    pass
