from PyQt5.QtWidgets import (
    QWidget, QVBoxLayout, QHBoxLayout, QLabel, QSpinBox, QDoubleSpinBox,
    QComboBox, QSizePolicy, QCheckBox, QLineEdit, QPushButton, QMessageBox,
    QGridLayout, QGroupBox
    )
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QIcon

import pygimli as pg
from pygimli import show
from pygimli.meshtools import createMesh
from pygimli.mplviewer import drawMeshBoundaries


class MeshOptions(QWidget):
    """Graphical access to the options for meshing a poly file."""

    def __init__(self, parent=None):
        """
        Initialize the main functionality.
        """
        # call the super class to establish the functionality of a QWidget
        super(MeshOptions, self).__init__(parent)
        self.parent = parent  # TabHolder
        self.setupWidget()

        # connect the signals to their functions
        self.btn_savemesh.clicked.connect(self.parent.parent.saveMesh)
        self.btn_mesh.clicked.connect(self.clickedBtnMesh)

    def setupWidget(self):
        """Design the layout of the tab that holds the options for tetgen."""
        self.ipath = self.parent.parent.ipath + '/'
        # define the mesh quality regarding the inner angle in a cell
        mesh_quality = QGroupBox("Mesh Quality:")
        self.spb_mesh_quality = QDoubleSpinBox()
        self.spb_mesh_quality.setMinimum(10.00)
        self.spb_mesh_quality.setMaximum(34.00)
        self.spb_mesh_quality.setValue(30.00)
        self.spb_mesh_quality.setSingleStep(0.01)
        l1 = QHBoxLayout()
        l1.addWidget(self.spb_mesh_quality)
        l1.setContentsMargins(0, 0, 0, 0)
        mesh_quality.setLayout(l1)

        # define the maximum area a cell can adjust to
        cell_area = QGroupBox("Max. Cell Area:")
        self.spb_cell_area = QDoubleSpinBox()
        self.spb_cell_area.setValue(0.0)
        self.spb_cell_area.setSingleStep(0.01)
        l2 = QHBoxLayout()
        l2.addWidget(self.spb_cell_area)
        l2.setContentsMargins(0, 0, 0, 0)
        cell_area.setLayout(l2)

        # define the box to choose which refinement algorithm might be chosen
        self.mesh_refine = QGroupBox("Refinement:")
        self.mesh_refine.setCheckable(True)
        self.mesh_refine.setChecked(False)
        self.cbx_mesh_refine = QComboBox()
        self.cbx_mesh_refine.addItem("quadratic")
        self.cbx_mesh_refine.addItem("spatially")
        l3 = QHBoxLayout()
        l3.addWidget(self.cbx_mesh_refine)
        l3.setContentsMargins(0, 0, 0, 0)
        self.mesh_refine.setLayout(l3)

        # define the smoothness variable and the number of iterations
        self.smooth = QGroupBox("Smooth:")
        self.smooth.setCheckable(True)
        self.smooth.setChecked(False)
        # self.chbx_smooth = QCheckBox()
        self.cbx_smooth = QComboBox()
        self.cbx_smooth.addItem("node center")
        self.cbx_smooth.addItem("weighted node center")
        self.spb_smooth = QSpinBox()
        self.spb_smooth.setToolTip("number of iterations")
        # self.spb_smooth.setEnabled(False)
        self.spb_smooth.setMinimum(1)
        self.spb_smooth.setValue(5)
        l4 = QHBoxLayout()
        l4.addWidget(self.cbx_smooth)
        l4.addWidget(self.spb_smooth)
        l4.setContentsMargins(0, 0, 0, 0)
        self.smooth.setLayout(l4)

        # # define the set of switches to override the other adjustments and
        # # access tetgen directly through the commandline options
        # self.la_switches = QLabel("Switches:")
        # self.la_switches.setEnabled(False)
        # self.chbx_switches = QCheckBox()
        # self.chbx_switches.setEnabled(False)
        # self.le_switches = QLineEdit("-pzeAfaq31")
        # self.le_switches.setEnabled(False)
        # self.switches = None

        # button to save mesh
        self.btn_savemesh = QPushButton()
        self.btn_savemesh.setIcon(QIcon(self.ipath + 'save.svg'))
        self.btn_savemesh.setIconSize(QSize(24, 24))
        self.btn_savemesh.setFixedSize(QSize(28, 28))
        self.btn_savemesh.setToolTip('Save Poly figure')
        self.btn_savemesh.setEnabled(False)

        # the button to hit and run
        self.btn_mesh = QPushButton()
        self.btn_mesh.setIcon(QIcon(self.ipath + 'mesh.svg'))
        self.btn_mesh.setIconSize(QSize(24, 24))
        self.btn_mesh.setFixedSize(QSize(28, 28))
        self.btn_mesh.setEnabled(False)

        hbox_mesh = QHBoxLayout()
        hbox_mesh.addStretch(1)
        hbox_mesh.addWidget(self.btn_savemesh)
        hbox_mesh.addWidget(self.btn_mesh)
        hbox_mesh.setContentsMargins(0, 0, 0, 0)

        v1 = QVBoxLayout()
        v1.addWidget(mesh_quality)
        v1.addWidget(cell_area)
        v1.addWidget(self.mesh_refine)
        v1.addWidget(self.smooth)
        v1.addStretch(1)
        v1.addLayout(hbox_mesh)
        v1.setContentsMargins(0, 0, 0, 0)

        self.setLayout(v1)

    # def changedChbxMeshRefine(self):
    #     if self.chbx_mesh_refine.isChecked() is True:
    #         self.cbx_mesh_refine.setEnabled(True)
    #         self.mesh_refine = True
    #     else:
    #         self.cbx_mesh_refine.setEnabled(False)
    #         self.mesh_refine = False

    # def changedChbxSmooth(self):
    #     if self.chbx_smooth.isChecked() is True:
    #         self.cbx_smooth.setEnabled(True)
    #         self.spb_smooth.setEnabled(True)
    #         # self.smooth = True
    #         # self.cbx_smooth = int(self.cbx_smooth.currentText())
    #         # self.spb_smooth = self.spb_smooth.value()
    #         # print("%i, %i" % (self.cbx_smooth, self.spb_smooth))
    #     else:
    #         self.cbx_smooth.setEnabled(False)
    #         self.spb_smooth.setEnabled(False)
    #         # self.smooth = True
    #         # self.cbx_smooth = None
    #         # self.spb_smooth = None
    #         # print("%i, %i" % (self.cbx_smooth, self.spb_smooth))

    # def changedChbxSwitches(self):
    #     if self.chbx_switches.isChecked() is True:
    #         self.le_switches.setEnabled(True)
    #     else:
    #         self.le_switches.setEnabled(False)

    def clickedBtnMesh(self):
        """Gather all Parameters and generate Mesh."""
        _refine = self.cbx_mesh_refine
        if not self.mesh_refine.isChecked():
            self.refine_method = None
        elif self.mesh_refine.isChecked() and _refine.currentText() == "quadratic":
            self.refine_method = "createP2"
        elif self.mesh_refine.isChecked() and _refine.currentText() == "spatially":
            self.refine_method = "createH2"

        if not self.smooth.isChecked():
            self.smooth_method = None
        else:
            self.smooth_method = [
                self.cbx_smooth.currentIndex(), self.spb_smooth.value()]

        # if self.chbx_switches.isChecked() is False:
        #     self.switches = None
        # else:
        #     self.switches = self.le_switches.text()

        # self.parent.statusbar.showMessage("generating mesh...")
        gimod = self.parent.parent
        self.mesh = createMesh(
            gimod.toolbar.ph.poly,
            quality=self.spb_mesh_quality.value(),
            area=self.spb_cell_area.value(),
            smooth=self.smooth_method,
            # switches=self.switches
            )

        if self.mesh_refine is True and self.cbx_mesh_refine.currentText() == "quadratic":
            # self.parent.statusbar.showMessage("create quadratic...")
            self.mesh = self.mesh.createP2()

        elif self.mesh_refine is True and self.cbx_mesh_refine.currentText() == "spatially":
            # self.parent.statusbar.showMessage("create spatially...")
            self.mesh = self.mesh.createH2()

        # print(self.mesh)
        self.btn_savemesh.setEnabled(True)
        gimod.plotwindow.showMesh(self.mesh)

        # self.parent.statusbar.showMessage(str(self.mesh))
        # self.parent.mb_save_mesh.setEnabled(True)
        # self.showMesh()


if __name__ == '__main__':
    pass
