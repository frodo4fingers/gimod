import matplotlib.pyplot as plt
import numpy as np
import re

from PyQt5.QtWidgets import (
    QWidget, QVBoxLayout, QTreeWidget, QTreeWidgetItem, QComboBox, QLineEdit,
    QCheckBox, QDoubleSpinBox, QPushButton, QHBoxLayout
)
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSize, Qt

"""
TODO
add flat QPushButton next to each polygon name for localisation of poly in figure
"""

class ParameterTree(QWidget):

    def __init__(self, parent=None):
        super(ParameterTree, self).__init__(parent)
        self.parent = parent  # TabHolder
        self.gimod = self.parent.parent
        # button storage for coloring
        self.location_btns = []
        self.has_world = False
        self.setupWidget()

        # signals
        self.pt.itemExpanded.connect(self._resize)
        self.btn_refresh.clicked.connect(self.createPolyDict)
        gimod = self.parent.parent
        self.btn_saveplc.clicked.connect(gimod.createAndSavePLC)
        self.btn_plc.clicked.connect(gimod.toolbar.ph.createPLC)
        self.btn_undo.clicked.connect(self.gimod.toolbar.ph.undoLastPoly)
        self.btn_redo.clicked.connect(self.gimod.toolbar.ph.redoLastPoly)

    def setupWidget(self):
        """Design the Layout."""
        self.pt = QTreeWidget()
        self.pt.setAlternatingRowColors(True)
        self.pt.setColumnCount(2)
        self.pt.setHeaderLabels(["Type", "Parameter"])
        self.pt.setUniformRowHeights(True)
        self.createEmpty()

        # create buttons on the bottom
        self.ipath = self.parent.parent.ipath + '/'
        self.btn_refresh = QPushButton()
        self.btn_refresh.setIcon(QIcon(self.ipath + 'refresh.svg'))
        self.btn_refresh.setIconSize(QSize(24, 24))
        self.btn_refresh.setFixedSize(QSize(28, 28))
        self.btn_refresh.setToolTip('Apply made changes')

        self.btn_saveplc = QPushButton()
        self.btn_saveplc.setIcon(QIcon(self.ipath + 'save.svg'))
        self.btn_saveplc.setIconSize(QSize(24, 24))
        self.btn_saveplc.setFixedSize(QSize(28, 28))
        self.btn_saveplc.setToolTip('Save Poly figure')
        self.btn_saveplc.setEnabled(False)

        self.btn_plc = QPushButton()
        self.btn_plc.setIcon(QIcon(self.ipath + 'plc.svg'))
        self.btn_plc.setIconSize(QSize(24, 24))
        self.btn_plc.setFixedSize(QSize(28, 28))
        self.btn_plc.setToolTip('Apply made changes')
        self.btn_plc.setEnabled(False)

        self.btn_undo = QPushButton()
        self.btn_undo.setIcon(QIcon(self.ipath + 'undo.svg'))
        self.btn_undo.setIconSize(QSize(24, 24))
        self.btn_undo.setFixedSize(QSize(28, 28))
        self.btn_undo.setToolTip('Undo last created item')
        self.btn_undo.setEnabled(False)

        self.btn_redo = QPushButton()
        self.btn_redo.setIcon(QIcon(self.ipath + 'redo.svg'))
        self.btn_redo.setIconSize(QSize(24, 24))
        self.btn_redo.setFixedSize(QSize(28, 28))
        self.btn_redo.setToolTip('Redo last deleted item')
        self.btn_redo.setEnabled(False)

        toolbox = QHBoxLayout()
        toolbox.addWidget(self.btn_refresh)
        toolbox.addStretch(1)
        toolbox.addWidget(self.btn_undo)
        toolbox.addWidget(self.btn_redo)
        toolbox.addWidget(self.btn_saveplc)
        toolbox.addWidget(self.btn_plc)
        toolbox.setContentsMargins(0, 0, 0, 0)

        layout = QVBoxLayout()
        layout.addWidget(self.pt)
        layout.addLayout(toolbox)
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

    def createEmpty(self):
        """Initialize the PolyGroups."""
        # clear the whole tree
        self.pt.clear()
        # initialize an item for every possible polytool used
        self.all_world = QTreeWidgetItem()
        self.all_world.setText(0, "PolyWorld ({})".format(str(self.all_world.childCount())))
        self.all_rect = QTreeWidgetItem()
        self.all_rect.setText(0, "PolyRectangle ({})".format(str(self.all_rect.childCount())))
        self.all_circ = QTreeWidgetItem()
        self.all_circ.setText(0, "PolyCircle ({})".format(str(self.all_circ.childCount())))
        self.all_poly = QTreeWidgetItem()
        self.all_poly.setText(0, "Polygon ({})".format(str(self.all_poly.childCount())))
        self.all_line = QTreeWidgetItem()
        self.all_line.setText(0, "PolyLine ({})".format(str(self.all_line.childCount())))
        # add them all to the tree
        self.pt.addTopLevelItem(self.all_world)
        self.pt.addTopLevelItem(self.all_rect)
        self.pt.addTopLevelItem(self.all_circ)
        self.pt.addTopLevelItem(self.all_poly)
        self.pt.addTopLevelItem(self.all_line)
        self._resize()

    def addPolyParameters(self, kind, params):
        if kind == 'world':
            self.addWorld(params)
        elif kind == 'rectangle':
            self.addRectangle(params)
        elif kind == 'polygon':
            self.addPolygon(params)
        elif kind == 'line':
            self.addPolyLine(params)
        elif kind == 'circle':
            self.addPolyCircle(params)
        self.btn_plc.setEnabled(True)
        self.btn_plc.setDefault(True)
        self.btn_undo.setEnabled(True)
        self._resize()

    def addWorld(self, values):
        self.has_world = True
        n_worlds = self.all_world.childCount()
        item = QTreeWidgetItem()
        item.setText(0, 'world_{}'.format(n_worlds))
        # call the start lineedit
        sitem, sle = self._getLineEdit('start', values['start'])
        item.addChild(sitem)
        # call the end lineedit
        eitem, ele = self._getLineEdit('end', values['end'])
        item.addChild(eitem)
        # call the marker combobox
        mitem, new_box = self._getMarker()
        values['marker'] = new_box.currentText()
        item.addChild(mitem)
        # call the area item
        aitem, ale = self._getLineEdit('area', values['area'])
        item.addChild(aitem)
        # call the LineEdit for layers
        litem, lle = self._getLineEdit('layers', values['layers'])
        item.addChild(litem)
        # call the checkbox for worldMarker
        witem, cbx = self._getCheckBox('worldMarker', values['worldMarker'])
        item.addChild(witem)

        self.all_world.addChild(item)
        self.all_world.setText(0, "PolyWorld ({})".format(n_worlds + 1))
        self.all_world.setExpanded(True)

        # set the item widgets
        # NOTE: this needs to happen afterwards
        self.pt.setItemWidget(sitem, 1, sle)
        self.pt.setItemWidget(eitem, 1, ele)
        self.pt.setItemWidget(mitem, 1, new_box)
        self.pt.setItemWidget(aitem, 1, ale)
        self.pt.setItemWidget(litem, 1, lle)
        self.pt.setItemWidget(witem, 1, cbx)

    def addRectangle(self, values):
        n_rects = self.all_rect.childCount()
        item = QTreeWidgetItem()
        # NOTE: the item text will be overlayed by a button, it s a bit dirty but much more easy
        # and faster in the process of changing polys
        item.setText(0, 'rectangle_{}'.format(n_rects))
        loc_btn = self._getLocateButton('rectangle_{}'.format(n_rects))
        # call the start lineedit
        sitem, sle = self._getLineEdit('start', values['start'])
        item.addChild(sitem)
        # call the end lineedit
        eitem, ele = self._getLineEdit('end', values['end'])
        item.addChild(eitem)
        # call the position lineedit
        pitem, ple = self._getLineEdit('pos', values['pos'])
        # call the size lineedit
        siitem, sile = self._getLineEdit('size', values['size'])
        item.addChild(eitem)
        # call the marker combobox
        mitem, new_box = self._getMarker()
        values['marker'] = new_box.currentText()
        item.addChild(mitem)
        # call the area item
        aitem, ale = self._getLineEdit('area', values['area'])
        item.addChild(aitem)
        # call the checkbox for worldMarker
        bitem, ble = self._getLineEdit('boundaryMarker', values['boundaryMarker'])
        item.addChild(bitem)
        # call checkbox for leftdirection
        lditem, ldcbx = self._getCheckBox('leftDirection', values['leftDirection'])
        item.addChild(lditem)
        # call checkbox for IsHole
        ihitem, ihcbx = self._getCheckBox('isHole', values['isHole'])
        item.addChild(ihitem)
        # call checkbox for IsClosed
        icitem, iccbx = self._getCheckBox('isClosed', values['isClosed'])
        item.addChild(icitem)

        self.all_rect.addChild(item)
        self.all_rect.setText(0, "PolyRectangles ({})".format(n_rects + 1))
        self.all_rect.setExpanded(True)

        # set the item widgets
        # NOTE: this needs to happen afterwards
        self.pt.setItemWidget(item, 0, loc_btn)
        self.pt.setItemWidget(sitem, 1, sle)
        self.pt.setItemWidget(eitem, 1, ele)
        self.pt.setItemWidget(pitem, 1, ple)
        self.pt.setItemWidget(siitem, 1, sile)
        self.pt.setItemWidget(mitem, 1, new_box)
        self.pt.setItemWidget(aitem, 1, ale)
        self.pt.setItemWidget(bitem, 1, ble)
        self.pt.setItemWidget(lditem, 1, ldcbx)
        self.pt.setItemWidget(ihitem, 1, ihcbx)
        self.pt.setItemWidget(icitem, 1, iccbx)

    def addPolygon(self, values):
        n_polygons = self.all_poly.childCount()
        item = QTreeWidgetItem()
        # NOTE: the item text will be overlayed by a button, it s a bit dirty but much more easy
        # and faster in the process of changing polys
        item.setText(0, 'polygon_{}'.format(n_polygons))
        loc_btn = self._getLocateButton('polygon_{}'.format(n_polygons))
        # call the area item
        vitem, vle = self._getLineEdit('verts', values['verts'])
        item.addChild(vitem)
        # call the checkbox for worldMarker
        bitem, ble = self._getLineEdit('boundaryMarker', values['boundaryMarker'])
        item.addChild(bitem)
        # call checkbox for leftdirection
        lditem, ldcbx = self._getCheckBox('leftDirection', values['leftDirection'])
        item.addChild(lditem)
        # call the marker combobox
        mitem, new_box = self._getMarker()
        values['marker'] = new_box.currentText()
        item.addChild(mitem)
        # call the area item
        aitem, ale = self._getLineEdit('area', values['area'])
        item.addChild(aitem)
        # call checkbox for IsHole
        ihitem, ihcbx = self._getCheckBox('isHole', values['isHole'])
        item.addChild(ihitem)
        # call checkbox for IsClosed
        icitem, iccbx = self._getCheckBox('isClosed', values['isClosed'])
        item.addChild(icitem)

        self.all_poly.addChild(item)
        self.all_poly.setText(0, "Polygons ({})".format(n_polygons + 1))
        self.all_poly.setExpanded(True)

        # set the item widgets
        # NOTE: this needs to happen afterwards
        self.pt.setItemWidget(item, 0, loc_btn)
        self.pt.setItemWidget(vitem, 1, vle)
        self.pt.setItemWidget(bitem, 1, ble)
        self.pt.setItemWidget(lditem, 1, ldcbx)
        self.pt.setItemWidget(mitem, 1, new_box)
        self.pt.setItemWidget(aitem, 1, ale)
        self.pt.setItemWidget(ihitem, 1, ihcbx)
        self.pt.setItemWidget(icitem, 1, iccbx)

    def addPolyLine(self, values):
        n_lines = self.all_line.childCount()
        item = QTreeWidgetItem()
        # NOTE: the item text will be overlayed by a button, it s a bit dirty but much more easy
        # and faster in the process of changing polys
        item.setText(0, 'line_{}'.format(n_lines))
        loc_btn = self._getLocateButton('line_{}'.format(n_lines))
        # call the start lineedit
        sitem, sle = self._getLineEdit('start', values['start'])
        item.addChild(sitem)
        # call the end lineedit
        eitem, ele = self._getLineEdit('end', values['end'])
        item.addChild(eitem)
        # call the segments lineedit
        seitem, sele = self._getLineEdit('segments', values['segments'])
        item.addChild(seitem)
        # call the checkbox for worldMarker
        bitem, ble = self._getLineEdit('boundaryMarker', values['boundaryMarker'])
        item.addChild(bitem)
        # call checkbox for leftdirection
        lditem, ldcbx = self._getCheckBox('leftDirection', values['leftDirection'])
        item.addChild(lditem)

        self.all_line.addChild(item)
        self.all_line.setText(0, "PolyLines ({})".format(n_lines + 1))
        self.all_line.setExpanded(True)

        # set the item widgets
        # NOTE: this needs to happen afterwards
        self.pt.setItemWidget(item, 0, loc_btn)
        self.pt.setItemWidget(sitem, 1, sle)
        self.pt.setItemWidget(eitem, 1, ele)
        self.pt.setItemWidget(seitem, 1, sele)
        self.pt.setItemWidget(bitem, 1, ble)
        self.pt.setItemWidget(lditem, 1, ldcbx)

    def addPolyCircle(self, values):
        n_circles = self.all_circ.childCount()
        item = QTreeWidgetItem()
        # NOTE: the item text will be overlayed by a button, it s a bit dirty but much more easy
        # and faster in the process of changing polys
        item.setText(0, 'circle_{}'.format(n_circles))
        loc_btn = self._getLocateButton('circle_{}'.format(n_circles))
        # call the radius lineedit
        pitem, ple = self._getLineEdit('radius', values['radius'])
        item.addChild(pitem)
        # call the position lineedit
        poitem, pole = self._getLineEdit('pos', values['pos'])
        item.addChild(poitem)
        # call the segments lineedit
        seitem, sele = self._getLineEdit('segments', values['segments'])
        item.addChild(seitem)
        # call the start lineedit
        sitem, sle = self._getPiSpinBox('start', values['start'])
        item.addChild(sitem)
        # call the end lineedit
        eitem, ele = self._getPiSpinBox('end', values['end'])
        item.addChild(eitem)
        # call the marker combobox
        mitem, new_box = self._getMarker()
        values['marker'] = new_box.currentText()
        item.addChild(mitem)
        # call the area item
        aitem, ale = self._getLineEdit('area', values['area'])
        item.addChild(aitem)
        # call the checkbox for boundaryMarker
        bitem, ble = self._getLineEdit(
            'boundaryMarker', values['boundaryMarker'])
        item.addChild(bitem)
        # call checkbox for leftdirection
        lditem, ldcbx = self._getCheckBox('leftDirection', values['leftDirection'])
        item.addChild(lditem)
        # call checkbox for IsHole
        ihitem, ihcbx = self._getCheckBox('isHole', values['isHole'])
        item.addChild(ihitem)
        # call checkbox for IsClosed
        icitem, iccbx = self._getCheckBox('isClosed', values['isClosed'])
        item.addChild(icitem)

        self.all_circ.addChild(item)
        self.all_circ.setText(0, "PolyCircles ({})".format(n_circles + 1))
        self.all_circ.setExpanded(True)

        # set the item widgets
        # NOTE: this needs to happen afterwards
        self.pt.setItemWidget(item, 0, loc_btn)
        self.pt.setItemWidget(poitem, 1, pole)
        self.pt.setItemWidget(pitem, 1, ple)
        self.pt.setItemWidget(seitem, 1, sele)
        self.pt.setItemWidget(sitem, 1, sle)
        self.pt.setItemWidget(eitem, 1, ele)
        self.pt.setItemWidget(mitem, 1, new_box)
        self.pt.setItemWidget(aitem, 1, ale)
        self.pt.setItemWidget(bitem, 1, ble)
        self.pt.setItemWidget(lditem, 1, ldcbx)
        self.pt.setItemWidget(ihitem, 1, ihcbx)
        self.pt.setItemWidget(icitem, 1, iccbx)

    def _getPiSpinBox(self, title, value):
        item = QTreeWidgetItem()
        item.setText(0, title)
        spb = QDoubleSpinBox()
        spb.setRange(0.00, 2*np.pi)
        spb.setSingleStep(0.01)
        spb.setValue(value)
        return item, spb

    def _getLineEdit(self, title, content):
        item = QTreeWidgetItem()
        item.setText(0, title)
        le = QLineEdit()
        le.setText(str(content))
        return item, le

    def _getCheckBox(self, title, state):
        item = QTreeWidgetItem()
        item.setText(0, title)
        cbx = QCheckBox()
        cbx.setTristate(False)
        cbx.setCheckState(bool(state))
        return item, cbx

    def _getMarker(self):
        """
        Create a box to chose the region marker and place it in the tree.
        
        Also updates all boxes so every polygon can get every marker.

        Parameters
        ----------
        item: QTreeWidgetItem
            Item in the tree that should receive the widget.
        col: int
            Column number of the widget where the ne box should be placed.
        """
        item = QTreeWidgetItem()
        item.setText(0, 'marker')

        if not hasattr(self, 'all_markerboxes'):
            self.all_markerboxes = []
        # create the new box and add it to the list
        new_box = QComboBox()
        self.all_markerboxes.append(new_box)

        n_boxes = len(self.all_markerboxes)
        for i, box in enumerate(self.all_markerboxes):
            box.clear()
            box.addItems([str(i) for i in range(n_boxes)])
            box.setCurrentIndex(i)

        return item, new_box

    def _getLocateButton(self, title):
        btn = QPushButton()
        btn.setIcon(QIcon(self.ipath + 'locate.svg'))
        btn.setText(title)
        btn.clicked.connect(self._connectToPH)
        self.location_btns.append(btn)
        self._repaintLocateButtons()

        return btn

    def _repaintLocateButtons(self):
        """color of the buttons according to the count of polygons."""
        n = len(self.location_btns)
        n = n + 1 if self.has_world else n
        cmap = plt.cm.get_cmap('Set3', n)
        cmap = [cmap(n) for n in range(n)]
        if self.has_world:
            cmap = cmap[1:]
        for k, btn in enumerate(self.location_btns):
            rgb = [int(i*255) for i in cmap[k][:-1]]
            btn.setStyleSheet("background-color: rgb({}, {}, {});".format(*rgb))

    def _connectToPH(self):
        _object = self.sender()
        oid = _object.text()
        self.gimod.toolbar.ph.setPatchActive(oid)

    def createPolyDict(self):
        """Read the ParameterTree and update all figures."""
        # get the dictionary where everything that came out of the
        # drawing process is stored. this needs to be updated.
        all_polys = self.gimod.toolbar.ph.all_polys

        root = self.pt.invisibleRootItem()
        child_count = root.childCount()  # >>> 5

        for i in range(child_count):
            # get every topLevelItem like PolyRectangle
            tli = root.child(i)
            for k in range(tli.childCount()):
                child = tli.child(k)
                # this then would be something like rectangle_0
                identifier = child.text(0)
                for l in range(child.childCount()):
                    # and here are following the arguments like start and end
                    cchild = child.child(l)
                    key = cchild.text(0)
                    # its not only text, but stuff stored in widgets
                    widget = self.pt.itemWidget(cchild, 1)
                    if isinstance(widget, QComboBox):
                        value = int(widget.currentText())

                    elif isinstance(widget, QLineEdit):
                        v = widget.text()
                        # list of values
                        if v.startswith('[['):
                            # get rid of [], leaving [], [], ..
                            v = v[1:-1]
                            v = [float(i) for i in re.findall(
                                r"[-+]?\d*\.\d+|\d+", v)]
                            value = [[v[i], v[i+1]] for i in range(0, len(v), 2)]

                        elif v.startswith('['):
                            v = v[1:-1]  # get rid of []
                            v = v.split(', ')
                            value = [float(x) for x in v]

                        else:
                            if v == '0.0':
                                value = float(v)
                            elif v != 'None':
                                # check for real int
                                _pts = v.split('.')
                                if len(_pts) > 1:
                                    if float(_pts[1]) > 0.:
                                        value = float(v)
                                    else:
                                        value = int(v)
                                else:
                                    value = int(v)
                            elif v == 'None':
                                value = None
                            else:
                                value = v

                    elif isinstance(widget, QCheckBox):
                        value = widget.isChecked()

                    elif isinstance(widget, QDoubleSpinBox):
                        value = widget.value()
                    # update the poly dictionary
                    all_polys[identifier]['data'][key] = value

        self.gimod.toolbar.ph.updateAndRedrawPatches()

    def _resize(self):
        self.pt.resizeColumnToContents(0)

    def _recount(self):
        """Adjust the Text of each TopLevelItem."""
        n_circles = self.all_circ.childCount()
        self.all_circ.setText(0, "PolyCircles ({})".format(n_circles))
        n_lines = self.all_line.childCount()
        self.all_line.setText(0, "PolyLines ({})".format(n_lines))
        n_polygons = self.all_poly.childCount()
        self.all_poly.setText(0, "Polygons ({})".format(n_polygons))
        n_rects = self.all_rect.childCount()
        self.all_rect.setText(0, "PolyRectangles ({})".format(n_rects))
        n_worlds = self.all_world.childCount()
        self.all_world.setText(0, "PolyWorld ({})".format(n_worlds))

    def deleteItemByPID(self, pid):
        root = self.pt.invisibleRootItem()
        child_count = root.childCount()  # >>> 5 for now
        for i in range(child_count):
            # get every topLevelItem like PolyRectangle
            tli = root.child(i)
            for k in range(tli.childCount()):
                child = tli.child(k)
                # this then would be something like rectangle_0
                identifier = child.text(0)
                if identifier == pid:
                    # index of the toplevelitem (which type the child is)
                    tidx = self.pt.indexOfTopLevelItem(tli)
                    # index of the child
                    cidx = tli.indexOfChild(child)
                    # remove child
                    item = tli.takeChild(cidx)
                    # remove the matching button from the list
                    if pid != 'world_0':
                        _ = self.location_btns.pop()
                    # remove the matching marker box from the list
                    if not 'line' in pid:
                        _ = self.all_markerboxes.pop()
                    # repaint the leftoverbuttons to match the patches
                    self._repaintLocateButtons()

        self._recount()


if __name__ == '__main__':
    pass
